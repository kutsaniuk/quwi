# nuxt

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## For editor development
Please run ```npm run dev``` with ENV variable EDITOR_PATH, which points to quwi-editor repo folder, i.e.
``` EDITOR_PATH=/home/user/quwi-editor npm run dev```
